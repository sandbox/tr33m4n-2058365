<?php
/**
 * @file
 * File module integration.
 */

/**
 * Implements hook_torrenttracker_field_type_info() on behalf of file.module.
 */
function file_torrent_tracker_field_type_info() {
	return array('file');
}
